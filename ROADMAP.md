# Roadmap of the shmdata documentation project

# March 2021

* (DONE) work on the website so that the pages which have already been created are accessible, and the pages which are work in progress (WIP) are not referred to through links on the website.

* (DONE) write a README for the docs

* (DONE) write a CONTRIBUTING that is directed at the docs, not the project

* (DONE) add a description of what the tutorial aims to accomplish

* (DONE) add a high-level, non-technical description of shmdata on the landing page

* make a list of potential next steps for the shmdata docs project

* plan ahead for April 2021 and May 2021
