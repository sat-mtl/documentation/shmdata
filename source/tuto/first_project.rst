.. _first tutorial:

First shmdata transmission
==========================

The following command sequence will let you check your shmdata
installation. It describes how to i) generate a video test signal into a
shmdata, ii) monitor it, and iii) read the video shmdata for display.
All these steps are performed by separate commands running on the same
computer.

Install `gst-launch`
--------------------

For this tutorial, you will need to install `gst-launch`, which is available in 
the `gstreamer1.0-tools` package on Ubuntu 20.04. Note that you can generally
use shmdata without `gst-launch`.

Transmit video through shmdata with GStreamer
---------------------------------------------

Create a shmdata in which raw video will be sent. The path to the
shmdata will be ``/tmp/video_shmdata``. This can be done in two
different ways: the general case makes use of ``gst-launch`` while it is
``gst-launch-1.0`` that is used on Ubuntu 20.04.

On Ubuntu 20.04
'''''''''''''''

.. code-block:: bash

   # generate a video test signal into a shmdata
   gst-launch-1.0 --gst-plugin-path=/usr/lib/gstreamer-1.0/ videotestsrc ! shmdatasink socket-path=/tmp/video_shmdata

Note: The command ``gst-launch`` allows for running `GStreamer
pipeline <https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html>`__
from command line. Here the pipeline is composed of two elements:
``videotestsrc`` that transmits its video stream to the ``shmdatasink``
element. The parameter ``socket-path`` indicates to the ``shmdatasink``
where the shmdata must be located. Finally, the ``--gst-plugin-path``
option tells ``gst-launch`` where it can find the ``shmdata`` GStreamer
elements (by default, shmdata GStreamer plugins are installed in
``/usr/lib/gstreamer-1.0/``).

Monitor a shmdata
-----------------

The ``sdflow`` utility is installed along with the shmdata library. It
prints the shmdata metadata once connected with the shmdata writer, and
then a line of information for each buffer pushed by the shmdata writer.
Keep the video shmdata running, and then from a new terminal type:

.. code-block:: bash

   # monitor a shmdata type and frame sizes 
   $ sdflow /tmp/video_shmdata 
   connected: type video/x-raw, format=(string)I420, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
   0    size: 115200    data: EBEBEBEBEBEBEBEBEBEBEBEBEBEBEB...
   1    size: 115200    data: EBEBEBEBEBEBEBEBEBEBEBEBEBEBEB...
   etc

Note that you can `monitor a shmadata framerate using pv and
sdflow <https://gitlab.com/sat-metalab/shmdata/-/tree/master/doc/monitor-framerate.md>`__.

Display video from the video shmdata
------------------------------------

With the video transmission still running (and optionally, the
``sdflow`` monitoring), open a new terminal window and display the video
using the following command:

On Ubuntu 20.04
'''''''''''''''

.. code-block:: bash
    
   # read the video shmdata and display its content into a window
   gst-launch-1.0 --gst-plugin-path=/usr/lib/gstreamer-1.0/ shmdatasrc socket-path=/tmp/video_shmdata ! xvimagesink 

Xvimagesink does not work in some remote server scenarios, however aasink provides a way to monitor a video feed using only ascii, even on an headless server instance via ssh.:

.. code-block:: bash
    
   # read the video shmdata and display its content as aasci video in terminal
   gst-launch-1.0 --gst-plugin-path=/usr/lib/gstreamer-1.0/ shmdatasrc socket-path=/tmp/video_shmdata ! aasink

.. figure:: ../_static/images/shmdata-principle-Diagram.png
   :alt: Shmdata principle diagram

   Shmdata principle diagram


